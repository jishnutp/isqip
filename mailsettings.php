<?php
$to          = $email;
$subject     = "Registration Details Of CEC ISQIP'16"; //Subject of the automated email
//email contents $fees must be defined before including
$resume_link = "http://isqip.cecieee.org/register/register_edit.php?id=" . $uniquekey;
$message     = "<!-- Inliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b -->
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; position: relative; height: 100%; margin: 0; padding: 0;\">
  <head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    <meta name=\"viewport\" content=\"width=device-width\" />
<!-- For development, pass document through inliner -->
  </head>
  <body style=\"font-size: 100%; font-family: Trebuchet, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; position: absolute; top: 50%; left: 50%; -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); -ms-transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); transform: translate(-50%, -50%); color: #34495e; text-align: center; width: 100% !important; height: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; background: #efefef; margin: 0; padding: 0;\" bgcolor=\"#efefef\"><style type=\"text/css\">
.btn:hover {
background-color: #27ae60;
}
.btn:focus {
background-color: #27ae60;
}
.btn:before {
content: \"\"; position: absolute; top: 50%; left: 50%; display: block; width: 0; padding-top: 0; border-radius: 100%; background-color: rgba(236, 240, 241, .3); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); -ms-transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); transform: translate(-50%, -50%);
}
.btn:active:before {
width: 120%; padding-top: 120%; transition: width .2s ease-out, padding-top .2s ease-out;
}
*:before {
box-sizing: border-box;
}
*:after {
box-sizing: border-box;
}
.btn.orange:hover {
background-color: #d35400;
}
.btn.orange:focus {
background-color: #d35400;
}
.btn.red:hover {
background-color: #c0392b;
}
.btn.red:focus {
background-color: #c0392b;
}
</style>
<table class=\"body-wrap\" style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; width: 100% !important; height: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; background: #efefef; margin: 0; padding: 0;\" bgcolor=\"#efefef\"><tr style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; margin: 0; padding: 0;\"><td class=\"container\" style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; display: block !important; clear: both !important; max-width: 580px !important; margin: 0 auto; padding: 0;\">

            <!-- Message start -->
            <table style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; width: 100% !important; border-collapse: collapse; margin: 0; padding: 0;\"><tr style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; margin: 0; padding: 0;\"><td align=\"center\" class=\"masthead\" style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; color: white; background: #e040fb; margin: 0; padding: 80px 0;\" bgcolor=\"#e040fb\">

                        <h1 style=\"font-size: 32px; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.25; box-sizing: border-box; max-width: 90%; text-transform: uppercase; margin: 0 auto; padding: 0;\">CECISQIP'16</h1>

                    </td>
                </tr><tr style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; margin: 0; padding: 0;\"><td class=\"content\" style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; background: white; margin: 0; padding: 30px 35px;\" bgcolor=\"white\">

                        <h2 style=\"font-size: 28px; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.25; box-sizing: border-box; font-weight: normal; margin: 0 0 20px; padding: 0;\">Hi $firstname $lastname,</h2>

                        <p style=\"font-size: 16px; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; font-weight: normal; margin: 0 0 20px; padding: 0;\">This is to notify you that you've successfully registered for CS ISQIP '16. ISQIP( IEEE Student Quality Improvement Programme ) is an initiative by IEEE Student Branch, CEC which aims at transforming students into industry ready professionals through skill development workshops and helping them to explore summer internship opportunities.<br style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; margin: 0; padding: 0;\" /><br style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; margin: 0; padding: 0;\" /></p>
                            <table style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; width: 100% !important; border-collapse: collapse; margin: 0; padding: 0;\"><tr style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; margin: 0; padding: 0;\"><td align=\"center\" style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; margin: 0; padding: 0;\">
                                    <p style=\"font-size: 16px; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; font-weight: normal; margin: 0 0 20px; padding: 0;\">
                                        <a class=\"btn red\" style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; position: relative; display: inline-block; overflow: hidden; outline: none; border-radius: 4px; box-shadow: 0 1px 4px rgba(0, 0, 0, .6); color: white; transition: background-color .3s; box-sizing: border-box; font-weight: bold; text-decoration: none; background: #e74c3c; margin: 30px auto; padding: 0; border-color: #e74c3c; border-style: solid; border-width: 10px 20px 8px;\">Total Fee:  $fee Rs</a>
                                    </p>
                                </td>
                            </tr></table><p style=\"font-size: 16px; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; font-weight: normal; margin: 0 0 20px; padding: 0;\">
The registration fee will be collected by the IEEE representatives from the respective classes. We kindly request you to pay the total sum before the classes close for study leave / summer holidays .

       <br style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; margin: 0; padding: 0;\" /><br style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; margin: 0; padding: 0;\" />    Events are more fun when you have friends with you to share and relive the awesome moments. Make sure that they don’t miss out on the unique opportunities. Share and let them know that you are going for ISQIP'16.
          </p>
                     
                        

                        <p style=\"font-size: 16px; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; font-weight: normal; margin: 0 0 20px; padding: 0;\">  As part of the learning process , we request you to prepare a resume .  The resume will act as a benchmark to help the participants realize the upward learning curve and will provide the essential prerequisites needed to attend the resume making session which is to be held as part of the event. There isn't any specific format to build the resume. Prepare it the way that seems best to you . Searching for templates on the web to prepare the resume is not encouraged. Be Unique !.</p>
                         <table style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; width: 100% !important; border-collapse: collapse; margin: 0; padding: 0;\"><tr style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; margin: 0; padding: 0;\"><td align=\"center\" style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; margin: 0; padding: 0;\">
                                    <p style=\"font-size: 16px; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; font-weight: normal; margin: 0 0 20px; padding: 0;\">
                                        <a href=\"$resume_link\" class=\"button\" style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; color: white; text-decoration: none; display: inline-block; font-weight: bold; border-radius: 4px; background: #71bc37; margin: 0; padding: 0; border-color: #71bc37; border-style: solid; border-width: 10px 20px 8px;\">Click Here To Upload Resume</a>
                                    </p>
                                </td>
                            </tr></table><p style=\"font-size: 16px; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; font-weight: normal; margin: 0 0 20px; padding: 0;\"><em style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; margin: 0; padding: 0;\"><h6 style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.25; box-sizing: border-box; margin: 0 0 20px; padding: 0;\">
                            Looking forward to seeing you there
                            </h6>-Regards,<br style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; margin: 0; padding: 0;\" /> ISQIP Registration team</em></p>

                    </td>
                </tr></table></td>
    </tr><tr style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; margin: 0; padding: 0;\"><td class=\"container\" style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; display: block !important; clear: both !important; max-width: 580px !important; margin: 0 auto; padding: 0;\">

            <!-- Message start -->
            <table style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; width: 100% !important; border-collapse: collapse; margin: 0; padding: 0;\"><tr style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; margin: 0; padding: 0;\"><td class=\"content footer\" align=\"center\" style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; background: white none; margin: 0; padding: 30px 35px;\" bgcolor=\"white\">
                        <p style=\"font-size: 14px; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; font-weight: normal; color: #888; text-align: center; margin: 0; padding: 0;\" align=\"center\">Sent by <a href=\"www.cecieee.org\" style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; color: #888; text-decoration: none; font-weight: bold; margin: 0; padding: 0;\">IEEE SB CEC</a> , College Of Engineering, Chengannur, Kerala</p>
                        <p style=\"font-size: 14px; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; font-weight: normal; color: #888; text-align: center; margin: 0; padding: 0;\" align=\"center\"><a href=\"isqip.cecieee.org\" style=\"font-size: 100%; font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; line-height: 1.65; box-sizing: border-box; color: #888; text-decoration: none; font-weight: bold; margin: 0; padding: 0;\">CECISQIP</a></p>
                    </td>
                </tr></table></td>
    </tr></table></body>
</html>
";
$header      = "From:donotreply@isqip.cecieee.org \r\n";
$header .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$retval = mail($to, $subject, $message, $header);
$msg    = 0;
if ($retval == true) {
    $msg = 1;
} else {
    $msg = 0;
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>AdminPanel - ISQIP'15</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body class="blurBg-false" style="background-color:#EBEBEB">



    <!-- Start form-->
    <link rel="stylesheet" href="signin_files/formoid1/formoid-metro-cyan.css" type="text/css" />
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <style>
        #msg {
            height: 200px; 
            overflow-y: scroll; 
        }

    </style>
    <script>
        $(document).ready(function() {
            var items = [],
                options = [];
            //Iterate all td's in second column
            $('.flat-table tbody tr td:nth-child(5)').each(function() {
                //add item to array
                items.push($(this).text());
            });

            //restrict array to unique items

            var items = $.unique(items);
            var x;
            for (x = 0; x < items.length; x++) {
                //items[x] = items[x].replace(/@.*\./, '@mailinator.');
            }
            var j = 0,
                i = (100 / items.length);
            $('.btn').click(function() {
                $('.sdm').removeClass("formoid-metro-cyan");

            });
            $('.rst').click(function() {
                $('.sdm').addClass("formoid-metro-cyan");
            });
            $('#send').click(function() {
                i = (100 / items.length);
                var bar = $('.w3-progressbar');
                $('#headp').text('Sending Mails Please Wait');
                $('.tett').hide();
                $('#pro').show();
                console.log(items.length);
                x = 0;
                for (x = 0; x < items.length; x++) {
                    $.post("mailgun.php", {
                            mai: items[x],
                            sub: $('#sub').val(),
                            msg: $('#text').val(),

                        },
                        function(data, status) {


                            $(bar).width(i + '%');
                            i = i + (100 / items.length);
                            console.log('x = ' + x);
                            console.log('j = ' + j);
                            j++;
                            $('#msg').append(data);
                            if (j == x - 1) {
                                $('#headp').text('Sending Mails Successfull');
                                $('.tett').show();

                            }


                        });
                }
            });
        });

    </script>
    <?php
if ($_POST['input'] == 'admin' && $_POST['password'] == 'isqip') {
    require("dbsettings.php");
    $db_host  = $mysqlihost;
    $db_user  = $mysqliuser;
    $db_pwd   = $mysqlipass;
    $resume   = $_POST['resume'];
    $tabl     = $_POST['workshop'];
    $database = $mysqlidb;
    $table    = 'prev';
    $xl       = $resume . $tabl;
    if ($xl == 'NOALL') {
        $query = "SELECT * FROM prev";
    } elseif ($xl == 'NOPHP') {
        $query = "SELECT * FROM prev WHERE cs_workshop='PHP'";
    } elseif ($xl == 'NOPython') {
        $query = "SELECT * FROM prev WHERE cs_workshop='Python'";
    } elseif ($xl == 'NOAdvanced JS') {
        $query = "SELECT * FROM prev WHERE cs_workshop='Advanced JS'";
    } elseif ($xl == 'NOAndroid') {
        $query = "SELECT * FROM prev WHERE cs_workshop='Android'";
    } elseif ($xl == 'YesALL') {
        $query = "SELECT * FROM prev WHERE upload!='Not Uploaded'";
    } elseif ($xl == 'YesPHP') {
        $query = "SELECT * FROM prev WHERE cs_workshop='PHP' AND upload!='Not Uploaded'";
    } elseif ($xl == 'YesPython') {
        $query = "SELECT * FROM prev WHERE cs_workshop='Python' AND upload!='Not Uploaded'";
    } elseif ($xl == 'YesAdvanced JS') {
        $query = "SELECT * FROM prev WHERE cs_workshop='Advanced JS' AND upload!='Not Uploaded'";
    } elseif ($xl == 'YesAndroid') {
        $query = "SELECT * FROM prev WHERE cs_workshop='Android' AND upload!='Not Uploaded'";
    } elseif ($xl == 'NOEC') {
        $query = "SELECT * FROM prev WHERE workshop!='CS' ";
    } elseif ($xl == 'YesEC') {
        $query = "SELECT * FROM prev WHERE workshop!='CS' AND upload!='Not Uploaded'";
    }
    echo '<form class="sdm formoid-metro-cyan" style="background-color:#FFFFFF;font-size:14px;font-family:\'Open Sans\',\'Helvetica Neue\',\'Helvetica\',Arial,Verdana,sans-serif;color:#666666;max-width:100%;min-width:150px" method="post" action="xls.php?download=' . $resume . $tabl . '"><div class="title"><h2>AdminPanel</h2></div>';
    if (!mysql_connect($db_host, $db_user, $db_pwd))
        die("Can't connect to database");
    if (!mysql_select_db($database))
        die("Can't select database");
    $result = mysql_query("$query");
    if (!$result) {
        die("Query to show fields from table failed");
    }
    $fields_num = mysql_num_fields($result);
    echo "<h2> </h2>";
    echo "<table class='flat-table'><tr>";
    for ($i = 0; $i < $fields_num; $i++) {
        $field = mysql_fetch_field($result);
        echo "<th>{$field->name}</th>";
    }
    echo "</tr>\n";
    while ($row = mysql_fetch_row($result)) {
        echo "<tr>";
        foreach ($row as $cell)
            echo "<td>$cell</td>";
        echo "</tr>\n";
    }
    echo "<div class=\"submit\"><input type=\"submit\" value=\"Download xls\"/></div><div class=\"submit\"></div></div></form>".'<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-controls-modal="#myModal" data-backdrop="static" data-keyboard="false"  data-target="#myModal">
    Send Emails To The Below Contestands as postmaster@cecieee.org
</button>';
    mysql_free_result($result);
    echo "</table>";
} else {
    echo "<h1> Loginfail </h1>";
}
?>
        <!-- Button trigger modal -->


        <p class="frmd"><a href="http://formoid.com/v29.php">jquery form</a> Formoid.com 2.9</p>
        <script type="text/javascript" src="signin_files/formoid1/formoid-metro-cyan.js"></script>
        <!-- Stop form-->



</body>
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="rst close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><div id="headp">Send Notification To All</div></h4>
            </div>
            <div class="modal-body">
                <div class="form-group tett">
                    <label for="email">Subject:</label>
                    <input type="text" class="form-control" id="sub">
                    <div class="form-group">
                        <label for="comment">Comment:</label>
                        <textarea class="form-control" rows="12" id="text"></textarea>
                    </div>
                </div>
                <div id="pro" hidden>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="w3-progress-container w3-round">
                                    <div class="w3-progressbar w3-round" style=""></div>
                                </div>
                                <body data-spy="scroll" data-target="#navbar-example">

                                    <div id="navbar-example">
                                        <ul id='msg'>

                                        </ul>
                                    </div>
                                    ...
                                </body>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer tett">
                    <button type="button" class="rst btn btn-default" data-dismiss="modal">Close</button>
                    <button id="send" type="button" class="btn btn-primary">Sendmail</button>
                </div>
            </div>
        </div>
    </div>
   

</html>

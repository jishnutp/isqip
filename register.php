<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Registration Form - ISQIP </title>
    <script type="text/javascript">
        function show() { document.getElementById('ieenor').style.display = 'block'; }
        function hide() { document.getElementById('ieenor').style.display = 'none'; }
        function showWork(elem) {

            if (elem.value == 'EC') {
                $('.element-select2').hide();
                $('#ecisqip').show();
                $('#csisqip').hide();
            } else if (elem.value == 0) {
                $('#ecisqip').hide();
                $('#csisqip').hide();
                $('#college').hide();
            } else if (elem.value == 'CS') {
                $('#ecisqip').hide();
                $('#csisqip').show();
            } else if (elem.value == 'CS&EC') {
                $('#ecisqip').show();
                $('#csisqip').show();
            } else if (elem.value == 'No'){
                $('#college').show();
            } else if (elem.value == 'CEC'){
                $('#college').hide();
            }
        }
    </script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body class="blurBg-false" style="background-color:#ffffff">



<!-- Start Formoid form-->
<link rel="stylesheet" href="register_files/formoid-solid-blue.css" type="text/css" />
<script type="text/javascript" src="register_files/jquery.min.js"></script>
<form  class="formoid-solid-blue" action="reg_complete.php" style="background-color:#ffffff;font-size:14px;font-family:'Roboto',Arial,Helvetica,sans-serif;color:#34495E;max-width:780px;min-width:150px" method="POST"  ><div class="title"><h2>Registration Form - ISQIP'15</h2><p style="margin-left: 27px"></p></div>
    <div class="element-name"><label class="title">Full Name :</label><span class="nameFirst"><input placeholder=" First Name" type="text" size="8" name="name[first]" /><span class="icon-place"></span></span><span class="nameLast"><input placeholder=" Last Name" type="text" size="14" name="name[last]" /><span class="icon-place"></span></span></div>
    <div class="element-select"><label class="title">Gender :<span class="required"></span></label><div class="item-cont"><div class="large"><span><select name="gender" required="required">

                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select><i></i><span class="icon-place"></span></span></div></div></div>
    <div class="element-select"><label class="title">Are You From CeC ? : <span class="required"></span></label><div class="item-cont"><div class="large"><span><select name="cec" required="required" onchange="showWork(this);">

                        <option value="0">Not Selected</option>
                        <option value="CEC" >Yes</option>
                        <option value="No" >No</option>
                    </select><i></i><span class="icon-place"></span></span></div></div></div>
    <div id="college" hidden="" class="element-input"><label class="title">College : </label><div class="item-cont"><input class="large" type="text" name="college" placeholder="CEC"/><span class="icon-place"></span></div></div>
    <div class="element-email"><label class="title">Email <span class="required">*</span></label><div class="item-cont"><input class="large" type="email" name="email" value="" required="required" placeholder="Email"/><span class="icon-place"></span></div></div>
    <div hidden="hidden" class="element-password"><label class="title">Password :</label><div class="item-cont"><input class="large" type="password" name="password" value="" placeholder="Password"/><span class="icon-place"></span></div></div>
    <div class="element-phone"><label class="title">Phone No. <span class="required">*</span></label><div class="item-cont"><input class="large" type="tel" pattern="[+]?[\.\s\-\(\)\*\#0-9]{3,}" maxlength="24" name="phone" required="required" placeholder="Phone" value=""/><span class="icon-place"></span></div></div>

    <div class="element-select"><label class="title">Branch :<span class="required">*</span></label><div class="item-cont"><div class="large"><span><select name="branch">

                        <option value="">Not Selected</option>
                        <option value="CS">CS</option>
                        <option value="EEE">EEE</option>
                        <option value="EI">EI</option>
                        <option value="EC">EC</option>
                        <option value="Others">Others</option></select><i></i><span class="icon-place"></span></span></div></div></div>
    <div class="element-select"><label class="title">Year :<span class="required">*</span></label><div class="item-cont"><div class="large"><span><select name="year" required="required">


                        <option value="">Not Selected</option>
                        <option value="2nd year">2nd year</option>
                        <option value="3rd year">3rd year</option>
                    </select><i></i><span class="icon-place"></span></span></div></div></div>
    <div class="element-select"><label class="title">EC OR CS Workshop ? : <span class="required"></span></label><div class="item-cont"><div class="large"><span><select name="ecorcs" required="required" onchange="showWork(this);">

                        <option value="0">Not Selected</option>
                        <option value="EC" >EC</option>
                        <option value="CS" >CS</option>
                        <option value="CS&EC" >CS And EC</option>
                    </select><i></i><span class="icon-place"></span></span></div></div></div>
    <div hidden="" id="ecisqip" class="element-select"><label class="title">EC Workshops :<span class="required">*</span></label><div class="item-cont"><div class="large"><span><select name="ecworkshop">

                        <option value="">Not Selected</option>
                        <option value="Raspberry & Scy Lab">Raspberry & Scy Lab</option>
                    </select><i></i><span class="icon-place"></span></span></div></div></div>
    <?php
    print_r('hellloooo');
    $php     = '<option value="PHP">PHP</option>';
    $python  = '<option value="Python">Python</option>';
    $js      = '<option value="Advanced JS">Advanced JS</option>';
    $android = '<option value="Android">Android</option>';
    //$limit set limit per choice
    $limit   = 30;
    require('dbsettings.php');
    mysqli_select_db($dbhandle, $mysqlidb) or die(mysqli_error($dbhandle));
    $sqlphp     = "SELECT * FROM prev WHERE cs_workshop='PHP' AND upload!='Not Uploaded'";
    $sqlpython  = "SELECT * FROM prev WHERE cs_workshop='Python' AND upload!='Not Uploaded'";
    $sqljs      = "SELECT * FROM prev WHERE cs_workshop='Advanced JS' AND upload!='Not Uploaded'";
    $sqlandroid = "SELECT * FROM prev WHERE cs_workshop='Android' AND upload!='Not Uploaded'";
    $result1 = mysqli_query($dbhandle, $sqlphp) or die(mysqli_error($dbhandle));
    $nophp = mysqli_num_rows($result1);
    $result2 = mysqli_query($dbhandle, $sqlpython) or die(mysqli_error($dbhandle));
    $nopython = mysqli_num_rows($result2);
    $result3 = mysqli_query($dbhandle, $sqljs) or die(mysqli_error($dbhandle));
    $nojs = mysqli_num_rows($result3);
    $result4 = mysqli_query($dbhandle, $sqlandroid) or die(mysqli_error($dbhandle));
    $noandroid   = mysqli_num_rows($result4);
    print_r('hellloooo2');
    //  echo "$nophp <br> $nopython <br> $nojs <br> $noandroid ";
    $sqlall      = "SELECT * FROM prev ";
    $sqlphp1     = "SELECT * FROM prev WHERE cs_workshop='PHP'";
    $sqlpython1  = "SELECT * FROM prev WHERE cs_workshop='Python'";
    $sqljs1      = "SELECT * FROM prev WHERE cs_workshop='Advanced JS'";
    $sqlandroid1 = "SELECT * FROM prev WHERE cs_workshop='Android'";
    $result112 = mysqli_query($dbhandle, $sqlall) or die(mysqli_error($dbhandle));
    $noall = mysqli_num_rows($result112);
    $result11 = mysqli_query($dbhandle, $sqlphp1) or die(mysqli_error($dbhandle));
    $nophp1 = mysqli_num_rows($result11);
    $result21 = mysqli_query($dbhandle, $sqlpython1) or die(mysqli_error($dbhandle));
    $nopython1 = mysqli_num_rows($result21);
    $result31 = mysqli_query($dbhandle, $sqljs1) or die(mysqli_error($dbhandle));
    $nojs1 = mysqli_num_rows($result31);
    $result41 = mysqli_query($dbhandle, $sqlandroid1) or die(mysqli_error($dbhandle));
    $noandroid1 = mysqli_num_rows($result41);
    //  echo "$nophp <br> $nopython <br> $nojs
    $php        = '<option value="PHP">PHP (' . $nophp1 . ' Registerd here)</option>';
    $python     = '<option value="Python">Python (' . $nopython1 . ' Registerd here)</option>';
    $js         = '<option value="Advanced JS">Advanced JS (' . $nojs1 . ' Registerd here)</option>';
    $android    = '<option value="Android">Android (' . $noandroid1 . ' Registerd here)</option>';
    print_r('hellloooo');
    if ($nophp >= $limit) {
        $php = '<option value="" disabled>PHP (No More Seats Here)</option>';
    }
    if ($nopython >= $limit) {
        $python = '<option value="" disabled>Python (No More Seats Here)</option>';
    }
    if ($nojs >= $limit) {
        $js = '<option value="" disabled>Advanced JS (No More Seats Here)</option>';
    }
    if ($noandroid >= $limit) {
        $android = '<option value="" disabled>Android (No More Seats Here)</option>';
    }
    echo '<div hidden="" id="csisqip" class="element-select"><label class="title">CS Workshops :<span class="required">*</span></label><div class="item-cont"><div class="large"><span><select name="csworkshop"  >

                        <option value="">Not Selected (Total ' . $noall . ' Registrations)</option>
                        ' . $php . '
                        ' . $python . '
                        ' . $js . '
                        ' . $android . '</select><i></i><span class="icon-place"></span></span></div></div></div>
    ';
    ?>
    <div class="element-select"><label class="title">Accomodation :<span class="required">*</span></label><div class="item-cont"><div class="large"><span><select name="accomodation" required="required">

                        <option value="Yes">Yes</option>
                        <option value="No">No</option></select><i></i><span class="icon-place"></span></span></div></div></div>
    <div class="element-radio"><label class="title">Are you :<span class="required">*</span></label>		<div class="column column3"><label><input type="radio" name="radio" value="NON-IEEE" required="required"  onclick="hide();" checked="checked"/><span>NON-IEEE</span></label></div><span class="clearfix"></span>
        <div class="column column3"><label><input type="radio" name="radio" value="IEEE-CS" required="required" onclick="show();" /><span>IEEE-CS  Member</span></label></div><span class="clearfix"></span>
        <div class="column column3"><label><input type="radio" name="radio" value="IEEE" required="required" onclick="show();"/><span>IEEE Member</span></label></div><span class="clearfix"></span>
    </div>
    <div id = "ieenor" hidden="" class="element-number"><label class="title">IEEE Membership No. :</label><div class="item-cont"><input class="large" type="text" min="0" max="1000000000" name="ieeenumber" placeholder="IEEE Membership Number" value=""/><span class="icon-place"></span></div></div>

    <br><br>
    <div class="element-separator"><hr><h3 class="section-break-title">Notes : </h3>
        <ul style="list-style-type:circle">
            <li>After Registration an email will be send to you to upload your resume</li>
        </ul>
    </div>
    <div class="submit"><input type="submit" value="Submit"/></div></form><p class="frmd"><a href="http://formoid.com/v29.php">html5 form</a> Formoid.com 2.9</p><script type="text/javascript" src="register_files/formoid1/formoid-solid-blue.js"></script>
<!-- Stop Formoid form-->



</body>
</html>

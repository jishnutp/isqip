<?php
// Connect to database server and select 
require("dbsettings.php");
if (!mysqli_select_db($dbhandle, $mysqlidb))
    die("Can't select database");
// retrive data which you want to export
$con = $dbhandle;
$xl  = $_GET['download'];
$header = '';
$data   = '';
if ($xl == 'NOALL') {
        $query = "SELECT * FROM prev";
    } elseif ($xl == 'NOPHP') {
        $query = "SELECT * FROM prev WHERE cs_workshop='PHP'";
    } elseif ($xl == 'NOPython') {
        $query = "SELECT * FROM prev WHERE cs_workshop='Python'";
    } elseif ($xl == 'NOAdvanced JS') {
        $query = "SELECT * FROM prev WHERE cs_workshop='Advanced JS'";
    } elseif ($xl == 'NOAndroid') {
        $query = "SELECT * FROM prev WHERE cs_workshop='Android'";
    } elseif ($xl == 'YesALL') {
        $query = "SELECT * FROM prev WHERE upload!='Not Uploaded'";
    } elseif ($xl == 'YesPHP') {
        $query = "SELECT * FROM prev WHERE cs_workshop='PHP' AND upload!='Not Uploaded'";
    } elseif ($xl == 'YesPython') {
        $query = "SELECT * FROM prev WHERE cs_workshop='Python' AND upload!='Not Uploaded'";
    } elseif ($xl == 'YesAdvanced JS') {
        $query = "SELECT * FROM prev WHERE cs_workshop='Advanced JS' AND upload!='Not Uploaded'";
    } elseif ($xl == 'YesAndroid') {
        $query = "SELECT * FROM prev WHERE cs_workshop='Android' AND upload!='Not Uploaded'";
    } elseif ($xl == 'NOEC') {
        $query = "SELECT * FROM prev WHERE workshop!='CS' ";
    } elseif ($xl == 'YesEC') {
        $query = "SELECT * FROM prev WHERE workshop!='CS' AND upload!='Not Uploaded'";
    }
$export = mysqli_query($con, $query) or die(mysqli_error($con));
// extract the field names for header 
while ($fieldinfo = mysqli_fetch_field($export)) {
    $header .= $fieldinfo->name . "\t";
}
// export data 
while ($row = mysqli_fetch_row($export)) {
    $line = '';
    foreach ($row as $value) {
        if ((!isset($value)) || ($value == "")) {
            $value = "\t";
        } else {
            $value = str_replace('"', '""', $value);
            $value = '"' . $value . '"' . "\t";
        }
        $line .= $value;
    }
    $data .= trim($line) . "\n";
}
$data = str_replace("\r", "", $data);
if ($data == "") {
    $data = "\nNo Record(s) Found!\n";
}
// allow exported file to download forcefully
date_default_timezone_set("Asia/Kolkata");
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=isqipxls_created_on_".date("Y-m-d/h-i-sa",time()).".xls");
header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$data";
?>